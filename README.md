# Ciudadanía Inteligente


## Install

- Install jekyll
```
gem install bundler jekyll
```
- Clone repository
```
git clone git@gitlab.com:ciudadaniai/ciudadaniai.git
```
- ` cd ciudadaniai` to enter the site
- ` bundle install` to install the gems
- Ready!
```
bundle exec jekyll serve --watch --baseurl=    
```
